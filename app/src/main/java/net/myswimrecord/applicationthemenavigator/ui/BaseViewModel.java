package net.myswimrecord.applicationthemenavigator.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import net.myswimrecord.applicationthemenavigator.Utils;

public class BaseViewModel extends ViewModel {

    protected MutableLiveData<String> mThemeName;

    public BaseViewModel() {
        mThemeName = new MutableLiveData<>();
        mThemeName.setValue(Utils.getTheme().getName());
    }

    public LiveData<String> getThemeName() {
        return mThemeName;
    }

    public void setThemeName(String themeName) {
        mThemeName.setValue(themeName);
    }
}
