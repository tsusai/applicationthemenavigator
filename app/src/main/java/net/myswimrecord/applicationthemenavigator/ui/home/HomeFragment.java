package net.myswimrecord.applicationthemenavigator.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.myswimrecord.applicationthemenavigator.R;
import net.myswimrecord.applicationthemenavigator.Utils;
import net.myswimrecord.applicationthemenavigator.ui.BaseViewModel;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private HomeViewModel homeViewModel;
    private BaseViewModel baseViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView tvTheme = root.findViewById(R.id.text_theme);

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        baseViewModel = new ViewModelProvider(this.getActivity()).get(BaseViewModel.class);
        baseViewModel.getThemeName().observe(this.getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                tvTheme.setText(s);
            }
        });


        FloatingActionButton floatingActionButton = root.findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(this);
        FloatingActionButton floatingActionButtonBack = root.findViewById(R.id.floatingActionButtonBack);
        floatingActionButtonBack.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
//        Utils.changeToTheme(this.getActivity(), Utils.AppTheme.THEME_APPCOMPAT);
//        baseViewModel.setThemeName(Utils.AppTheme.THEME_APPCOMPAT.getName());
        switch (v.getId()) {
            case R.id.floatingActionButton:
                baseViewModel.setThemeName(Utils.changeToNext(this.getActivity(),false));
                break;
            case R.id.floatingActionButtonBack:
                baseViewModel.setThemeName(Utils.changeToNext(this.getActivity(), true));
                break;
        }
    }
}