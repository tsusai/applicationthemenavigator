package net.myswimrecord.applicationthemenavigator;

import android.app.Activity;

public class Utils
{

    private static AppTheme sTheme = AppTheme.App_Theme;

    public enum AppTheme {

        App_Theme(R.style.AppTheme),
        Theme_AppCompat(R.style.Theme_AppCompat),
        Theme_AppCompat_DayNight(R.style.Theme_AppCompat_DayNight),
        Theme_AppCompat_DayNight_DarkActionBar(R.style.Theme_AppCompat_DayNight_DarkActionBar),
        Theme_AppCompat_Light(R.style.Theme_AppCompat_Light),
        Theme_AppCompat_Light_DarkActionBar(R.style.Theme_AppCompat_Light_DarkActionBar),
        ThemeOverlay_AppCompat_ActionBar(R.style.ThemeOverlay_AppCompat_ActionBar),
        ThemeOverlay_AppCompat_Dark(R.style.ThemeOverlay_AppCompat_Dark),
        ThemeOverlay_AppCompat_Dark_ActionBar(R.style.ThemeOverlay_AppCompat_Dark_ActionBar);

        int mResId;

        AppTheme(int resId) {
            mResId = resId;
        }

        public String getName() {
            return this.name();
        }
    }

    public static AppTheme getTheme() {
        return sTheme;
    }

    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, AppTheme theme) {
        sTheme = theme;
        activity.recreate();
    }
    public static String changeToNext(Activity activity, boolean isReverse) {

        AppTheme[] appThemes = AppTheme.values();
        sTheme = appThemes[(sTheme.ordinal()+(isReverse?(appThemes.length-1):1)) % appThemes.length];
        changeToTheme(activity, sTheme);
        return sTheme.getName();
    }
    /** Set the theme of the activity, according to the configuration. */
    public static void onActivityCreateSetTheme(Activity activity) {
        activity.setTheme(sTheme.mResId);
    }
}
